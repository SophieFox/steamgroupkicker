import steam.webauth as wa
from auth import *

import re, sys

user = wa.WebAuth(USERNAME, PASSWORD)

PAGE = 0

if len(sys.argv) > 1:
	PAGE = int(sys.argv[1])

pattern = re.compile("ManageMembers_Kick\( '(\d*)',")

def get_members(user):
	page = 1
	s = user.session
	ret = []
	for page in range(PAGE + 1, PAGE + 3):
		print("Fetching page " + str(page))
		r = s.get("http://steamcommunity.com/groups/%s/membersManage/?p=%d" % (GROUP, page))
		r.raise_for_status()
		n = pattern.findall(r.text)
		if n:
			ret.extend(n)
	return ret
	
def kick_members(user, members):
	s = user.session
	s.headers = {"Content-Type": "application/x-www-form-urlencoded"}
	sessionid = s.cookies.get("sessionid", domain="steamcommunity.com")
	num_members = len(members)

	for i in range(num_members):
		m = members[i]
		print("Kicking %s (%d/%d)" % (str(m), i+1, num_members))
		data=[("sessionID", sessionid), ("action" , "kick"), ("memberId", m), ("queryString", "")]
		r = s.post("http://steamcommunity.com/groups/%s/membersManage" % (GROUP), data=data)
		r.raise_for_status()


def main():
	global user
	print("Trying to login as:", USERNAME)
	try:
		user.login()
	except wa.CaptchaRequired:
		print ("Please solve the following CAPTCHA:\n", user.captcha_url)
		s = input('CAPTCHA: ')
		user.login(captcha=s)
	except wa.EmailCodeRequired:
		s = input('Email Code: ')
		user.login(email_code=s)
	except wa.TwoFactorCodeRequired:
		s = input('2-Factor Code: ')
		user.login(twofactor_code=s)

	print("Logged in as:", user.steam_id)

	members = True
	while members:
		print("Fetching members")
		members = get_members(user)
		print("Kicking members")
		kick_members(user, members)
	print("done")

if __name__ == '__main__':
	main()
